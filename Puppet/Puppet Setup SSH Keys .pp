$public_key =  'AAAAB3NzaC1yc2EAAAADAQABAAABAQC/rkHWxWFvhGbQy/pCJ6AETtEeDLShAAed9urKKit80x0oZL4Ea32FjovsibZd2VEmc85W0HgGeh0cy7wJsrd+2IE4cNevBEoU/hwm30Z2pPbOWVlEbZpllFJ6NAV5J1JYJcPkN/Ve997kLAOIwYxGTkVXbI0zyscuUGjQctFEFh/YGNZ/iNSCnQUP/fRYdPjwrpegI997OSqTB8M0VfdOJ8Ww53uwShFSX4EYg2QBxCqEF9C13o0knVwxh5IsWZ9sIJnkKvW/HjW3p59oSdjoDckNu1SBv10HFfh8rB1vJo/c+kvDHWBsqGGJRCFAE5kXv+5UeaVnGbDlO2GoVYdT'

class ssh_node1 {

   ssh_authorized_key { 'tony@stapp01':

     ensure => present,

    user   => 'tony',

     type   => 'ssh-rsa',

     key    => $public_key,

   }

 }

 class ssh_node2 {

   ssh_authorized_key { 'steve@stapp02':

     ensure => present,

     user   => 'steve',

     type   => 'ssh-rsa',

     key    => $public_key,

   }

 }

 class ssh_node3 {

   ssh_authorized_key { 'banner@stapp03':

     ensure => present,

     user   => 'banner',

     type   => 'ssh-rsa',

     key    => $public_key,

   }

 }

 node stapp01.stratos.xfusioncorp.com {

   include ssh_node1

 }

 node stapp02.stratos.xfusioncorp.com {

   include ssh_node2

 }

 node stapp03.stratos.xfusioncorp.com {

   include ssh_node3

 }